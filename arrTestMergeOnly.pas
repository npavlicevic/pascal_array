program arrTestMergeOnly;
  uses sysutils, arr;
  var
    n, i: integer;
    a,b,c: array of int64;
  begin
    n := StrToInt(ParamStr(1));
    SetLength(a, n);
    SetLength(b, n);
    SetLength(c, 2*n);
    Randomize;
    for i := 0 to n-1 do
      begin
        a[i] := Random(maxint);
        b[i] := Random(maxint);
        write(IntToStr(a[i]), ' .', IntToStr(b[i]), ' ');
      end;
    writeln(' ');
    ArrSortComb(@a[0], n, SizeOf(int64), @CompareInt64);
    ArrSortComb(@b[0], n, SizeOf(int64), @CompareInt64);
    for i := 0 to n-1 do
      write(IntToStr(a[i]), ' .', IntToStr(b[i]), ' ');
    writeln(' ');
    ArrMergeOnly(@a[0], @b[0], @c[0], n, SizeOf(int64), @CompareInt64);
    for i := 0 to 2*n - 1 do
      write(IntToStr(c[i]), ' ');
    writeln(' ');
  end.
