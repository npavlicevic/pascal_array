program arrTestMerge;
  uses sysutils, arr;
  var
    n,i: integer;
    a,b: array of int64;
  begin
    n := StrToInt(ParamStr(1));
    SetLength(a, n);
    SetLength(b, n);
    Randomize;
    for i := 0 to n-1 do
      begin
        a[i] := Random(maxint);
        write(IntToStr(a[i]), ' ');
      end;
    writeln(' ');
    ArrSortMerge(@b[0],@a[0],0,n-1,SizeOf(int64),@CompareInt64);
    for i := 0 to n-1 do
      write(IntToStr(a[i]), ' ');
    writeln(' ');
  end.
