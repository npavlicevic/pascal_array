Implementation of array algorithms for free pascal (fpc).
  
To compile type make all.
  
Algorithms included (alongside number of steps)
  
1. Bubble sort
  
         2
        n
  
2. Insert sort
  
         2
        n
  
3. Comb sort
  
                    2
        less than n
  
4. Shell sort
  
                    2
        less then n
  
5. Merge sort
  
          n log   n
                2
  
6. Binary find
  
          log   n
              2
  
7. Reverse
  
        n
  
8. Merge
  
        n
