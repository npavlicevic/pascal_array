{$MACRO ON}
unit arr;
  interface
    uses cmem, sysutils;
    type
      TCompare = function(a,b: pointer): integer;
    function CompareInt64(a,b: pointer): integer;
    function ArrFind(a: pointer;n,l,h,bytes: integer;compare: TCompare): integer;
    procedure ArrSortComb(a: pointer;n,bytes: integer;compare: TCompare);
    procedure ArrSortBubble(a: pointer;n,gap,bytes: integer;compare: TCompare);
    procedure ArrSortShell(a: pointer;n,bytes: integer;compare: TCompare);
    procedure ArrSortInsert(a: pointer;n,bytes,gap: integer;compare: TCompare);
    procedure ArrSortMerge(dest,src: pointer;l,h,bytes: integer;compare: TCompare);
    procedure ArrMerge(dest,src: pointer;l,mid,h,bytes: integer;compare: TCompare);
    procedure BytesCopy(dest,source: pointer;bytes,i,j: integer);
    procedure ArrCycle(a: pointer;i,j,bytes: integer);
    procedure ArrMergeOnly(a,b,c: pointer;n,bytes: integer; compare: TCompare);
    procedure ArrReverse(a: pointer;n,bytes: integer);
  implementation
    {
      CompareInt64
    }
    function CompareInt64(a,b: pointer): integer;
    var
      i,j: pint64;
    begin
      i := PInt64(a);
      j := PInt64(b);
      if (i^ = j^) then
        CompareInt64 := 0
      else if (i^ > j^) then
        CompareInt64 := 1
      else
        CompareInt64 := -1;
    end;
    {
      ArrSortComb

      comb sort
    }
    procedure ArrSortComb(a: pointer;n,bytes: integer;compare: TCompare);
    var
      i,j,gap,c: integer;
      sorted: boolean;
    begin
      gap := n-1;
      sorted := false;
      while sorted = false do
        begin
          if gap > 1 then
            sorted := false
          else
            begin
              gap := 1;
              sorted := true;
            end;
          i := 0;
          for j := gap to n-1 do
            begin
              c := compare(a+i*bytes,a+j*bytes);
              if c = 1 then 
                begin
                  ArrCycle(a,i,j,bytes);
                  sorted := false;
                end;
              i := i+1;
            end;
          gap := Trunc(gap/1.2);
        end;
    end;
    {
      ArrSortBubble
    }
    procedure ArrSortBubble(a: pointer;n,gap,bytes: integer;compare: TCompare);
    var
      i,j,c: integer;
      change: boolean;
    begin
      change := true;
      while change do
      begin
        i := 0;
        change := false;
        for j := gap to n-1 do
          begin
            c := compare(a+i*bytes,a+j*bytes);
            if c = 1 then
              begin
                ArrCycle(a,i,j,bytes);
                change := true;
              end;
            i := i+1;
          end;
        n := n-1;
      end;
    end;
    {
      ArrFind
    }
    function ArrFind(a: pointer;n,l,h,bytes: integer;compare: TCompare): integer;
    var
      mid,c: integer;
    begin
      while l < h do
        begin
          mid := Trunc((l+h)/2);
          c := compare(a+n*bytes,a+mid*bytes);
          if c = 0 then
            begin
              ArrFind := mid;
              exit;
            end
          else if c = 1 then
            l := mid+1
          else
            h := mid-1;
        end;
      ArrFind := -1;
    end;
    {
      ArrSortShell
    }
    procedure ArrSortShell(a: pointer;n,bytes: integer;compare: TCompare);
    var
      gap: integer;
    begin
      gap := n-1;
      while gap > 0 do
      begin
        ArrSortInsert(a,n,bytes,gap,compare);
        gap := Trunc(gap/1.2);
      end;
    end;
    {
      ArrSortInsert
    }
    procedure ArrSortInsert(a: pointer; n,bytes,gap: integer;compare: TCompare);
    var
      i,j,c: integer;
    begin
      i := gap;
      while i < n do
      begin
        j := i;
        while j >= gap do
        begin
          c := compare(a+(j-gap)*bytes,a+j*bytes);
          if c = 1 then
            ArrCycle(a,j-gap,j,bytes);
          j := j-gap;
        end;
        i := i+gap;
      end;
    end;
    {
      ArrSortMerge
    }
    procedure ArrSortMerge(dest,src: pointer;l,h,bytes: integer;compare: TCompare);
    var
      mid: integer;
    begin
      mid := Trunc((l+h)/2);
      if l < h then
        begin
          ArrSortMerge(dest,src,l,mid,bytes,compare);
          ArrSortMerge(dest,src,mid+1,h,bytes,compare);
          ArrMerge(dest,src,l,mid,h,bytes,compare);
        end;
    end;
    {
      ArrMerge
    }
    procedure ArrMerge(dest,src: pointer;l,mid,h,bytes: integer;compare: TCompare);
    var
      one,other,third,fourth,fifth,cmp: integer;
      {$define DirectionZeroL1:=BytesCopy(dest,src,bytes,fifth,one);}
      {$define DirectionZeroL2:=one:=one+1;}
      {$define DirectionOneL1:=BytesCopy(dest,src,bytes,fifth,third);}
      {$define DirectionOneL2:=third:=third+1;}
    begin
      if l < h then
        begin
          one := l;
          other := mid;
          third := mid+1;
          fourth := h;
          fifth := l;
          while (one <= other) and (third <= fourth) do
            begin
              cmp := compare(src+third*bytes,src+one*bytes);
              if cmp = 1 then
                begin
                  DirectionZeroL1
                  DirectionZeroL2
                end
              else if cmp = -1 then
                begin
                  DirectionOneL1
                  DirectionOneL2
                end
              else
                begin
                  DirectionZeroL1
                  DirectionZeroL2
                  DirectionOneL2
                end;
              fifth := fifth+1;
            end;
          {check if left list done}
          while (one <= other) do
            begin
              BytesCopy(dest,src,bytes,fifth,one);
              one := one+1;
              fifth := fifth+1;
            end;
          {check if right list done}
          while (third <= fourth) do
            begin
              BytesCopy(dest,src,bytes,fifth,third);
              third := third+1;
              fifth := fifth+1;
            end;
          for fifth := l to h do
            BytesCopy(src,dest,bytes,fifth,fifth);
        end;
    end;
    {
      BytesCopy

      copy bytes from source to destination
    }
    procedure BytesCopy(dest,source: pointer;bytes,i,j: integer);
    begin
      StrLCopy(dest+(i*bytes), source+(j*bytes), bytes);
    end;
    {
      ArrCycle

      cycle items
    }
    procedure ArrCycle(a: pointer;i,j,bytes: integer);
    var
      temp: pointer;
    begin
      temp := Malloc(bytes);
      BytesCopy(temp,a,bytes,0,j);
      BytesCopy(a,a,bytes,j,i);
      BytesCopy(a,temp,bytes,i,0);
      Free(temp);
    end;
    {
      ArrMergeOnly
    }
    procedure ArrMergeOnly(a,b,c: pointer;n,bytes: integer; compare: TCompare);
    var
      l,r,m,cmp : integer;
    begin
      l := 0;
      r := 0;
      m := 0;
      while (l < n) and (r < n) do
        begin
          cmp := compare(a+l*bytes,b+r*bytes);
          if cmp = 1 then
            begin
              BytesCopy(c,b,bytes,m,r);
              r := r+1;
            end
          else if cmp = -1 then
            begin
              BytesCopy(c,a,bytes,m,l);
              l := l+1;
            end;
          if cmp = 0 then
            begin
              BytesCopy(c,b,bytes,m,r);
              l := l+1;
              r := r+1;
            end;
          m := m+1;
        end;
      while l < n do
        begin
          BytesCopy(c,a,bytes,m,l);
          l := l+1;
          m := m+1;
        end;
      while r < n do
        begin
          BytesCopy(c,b,bytes,m,r);
          r := r+1;
          m := m+1;
        end;
    end;
    {
      ArrReverse

      reverse array
    }
    procedure ArrReverse(a: pointer;n,bytes: integer);
    var
      lo,hi,mid: integer;
    begin
      hi:=n-1;
      mid:=Trunc(n/2);
      for lo:=0 to mid-1 do
        begin
          ArrCycle(a,lo,hi,bytes);
          hi:=hi-1;
        end;
    end;
end.
