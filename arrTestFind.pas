program arrTestFind;
  uses sysutils, arr;
  var
    n,max,find,i: integer;
    a: array of int64;
  begin
    n := StrToInt(ParamStr(1));
    max := StrToInt(ParamStr(2));
    find := StrToInt(ParamStr(3));
    SetLength(a,n+1);
    a[n] := find;
    Randomize;
    for i := 0 to n-1 do
      begin
        a[i] := Random(max);
        write(IntToStr(a[i]), ' ');
      end;
    writeln(' ');
    ArrSortComb(@a[0], n, SizeOf(int64), @CompareInt64);
    for i := 0 to n-1 do
      write(IntToStr(a[i]), ' ');
    writeln(' ');
    writeln('position: ', IntToStr(ArrFind(@a[0],n,0,n-1,SizeOf(int64),@CompareInt64)));
  end.
