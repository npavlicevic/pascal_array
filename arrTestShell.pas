program arrTestShell;
  uses sysutils, arr;
  var
    n, i: integer;
    a: array of int64;
  begin
    n := StrToInt(ParamStr(1));
    SetLength(a, n);
    Randomize;
    for i := 0 to n-1 do
      begin
        a[i] := Random(maxint);
        write(IntToStr(a[i]), ' ');
      end;
    writeln(' ');
    ArrSortShell(@a[0], n, SizeOf(int64), @CompareInt64);
    for i := 0 to n-1 do
      write(IntToStr(a[i]), ' ');
    writeln(' ');
  end.
