#
# Makefile
# array unit and programs
# 
CC=fpc
FLAGS=-g
ARR=arr.pas
ARR_TEST=arrTest.pas
ARR_TEST_MERGE=arrTestMerge.pas
ARR_TEST_BUBBLE=arrTestBubble.pas
ARR_TEST_FIND=arrTestFind.pas
ARR_TEST_MERGE_ONLY=arrTestMergeOnly.pas
ARR_TEST_INSERT=arrTestInsert.pas
ARR_TEST_SHELL=arrTestShell.pas
ARR_TEST_SHELL=arrTestReverse.pas

all: lib arrTest arrTestMerge arrTestBubble arrTestFind arrTestMergeOnly arrTestInsert arrTestShell arrTestReverse

lib: ${ARR}
	${CC} ${FLAGS} ${ARR}
arrTest: ${ARR} ${ARR_TEST}
	${CC} ${FLAGS} ${ARR_TEST}
arrTestMerge: ${ARR} ${ARR_TEST_MERGE}
	${CC} ${FLAGS} ${ARR_TEST_MERGE}
arrTestBubble: ${ARR} ${ARR_TEST_BUBBLE}
	${CC} ${FLAGS} ${ARR_TEST_BUBBLE}
arrTestFind: ${ARR} ${ARR_TEST_FIND}
	${CC} ${FLAGS} ${ARR_TEST_FIND}
arrTestMergeOnly: ${ARR} ${ARR_TEST_MERGE_ONLY}
	${CC} ${FLAGS} ${ARR_TEST_MERGE_ONLY}
arrTestInsert: ${ARR} ${ARR_TEST_INSERT}
	${CC} ${FLAGS} ${ARR_TEST_INSERT}
arrTestShell: ${ARR} ${ARR_TEST_SHELL}
	${CC} ${FLAGS} ${ARR_TEST_SHELL}
arrTestReverse: ${ARR} ${ARR_TEST_REVERSE}
	${CC} ${FLAGS} ${ARR_TEST_REVERSE}
clean:
	rm arrTest arrTestMerge arrTestBubble arrTestFind arrTestMergeOnly arrTestInsert arrTestShell arrTestReverse
